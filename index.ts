import { Application, config, Context, Router, Status, Database } from "./deps.ts";

const env = config();
const port = parseInt(env.PORT);

interface UwU {
  id: string;
}

const db = new Database<UwU>({
  path: './data/UwU.json',
  pretty: true,
  autoload: true,
  autosave: true,
  optimize: true,
  immutable: true,
  // validator: (document: any) => {}
});

/* interface owo {
  id: string;
  body: string;
} */

// const OwO = new Map<string, owo>()
/* OwO.set("OwO", {
  id: "owo",
  body: "What's this?",
}); */

const router = new Router();
router
  .get('/', (ctx: Context) => {
    ctx.response.body = "What brings you here?\n\n\nEndpoints:\n/api";
  })
  .get('/api', (ctx: Context) => {
    ctx.response.body = "Endpoints:\n/api/uwu\n/api/uwu/:id";
  })
  .get('/api/uwu', async (ctx: Context) => {
    // ctx.response.body = Array.from(OwO.values());
    ctx.response.body = await db.findMany();
  })
  .post('/api/uwu', async (ctx: Context) => {
    if (!ctx.request.hasBody) {
      ctx.throw(Status.BadRequest, "UwU Bad Request [id must be a string]");
    }
    const body = ctx.request.body();
    // let owo = Partial<owo> | undefined;
    let uwu
    if (body.type === "json") {
      uwu = await body.value;
    }
    if (uwu) {
      ctx.assert(
        uwu.id && typeof uwu.id === "string",
        Status.BadRequest,
        "UwU Bad Request",
      );
      // OwO.set(owo.id, owo as owo);
      db.insertOne(uwu);
      console.log("an UwU got posted!");
      ctx.response.status = Status.OK;
      ctx.response.body = uwu;
      ctx.response.type = "json";
      return;
    }
    ctx.throw(Status.BadRequest, "UwU Bad Request");
  })
  .delete<{ id: string }>("/api/uwu/:id", async (ctx) => {
    if (ctx.params.id) {
      ctx.response.body = await db.deleteOne({ id: ctx.params.id });
      console.log("an UwU was deleted!");
    } else { 
      return notFound(ctx);
    } 
  })
  .get<{ id: string }>("/api/uwu/:id", async (ctx) => {
    // if (ctx.params && await db.getCollection<owo>('OwO'))
    if (ctx.params.id) {
      ctx.response.body = await db.findOne({ id: ctx.params.id });
    } else { 
      return notFound(ctx);
    } 
    });

function notFound(ctx: Context) {
  ctx.response.status = Status.NotFound;
  ctx.response.body =
    `<html><body><h1>404 - Not Found</h1><p>Path <code>${ctx.request.url}</code> not found.`;
}

const app = new Application();
app.use(router.routes());
app.use(router.allowedMethods());
app.use(notFound);

console.log(`Listening on port ${port} | http://127.0.0.1:${port} | /uwu (GET) | /uwu/:id (DELETE and GET)`);
await app.listen({ hostname: "127.0.0.1", port: port });
